function initAptekaSlider(){
	$('.s-apteka-slider').each(function() {
		var el = $(this); //общий контейнер, в котором лежат и слайдер, и стрелки
		var slider = el.find('.s-apteka-slider');
		var nextArrow = el.find('.slider__owl-next');
		var prevArrow = el.find('.slider__owl-prev');
		
		slider.owlCarousel({
			// параметыcenter: true,
			
			loop:false,
		    margin:3,
		    dots: false,
		    nav:false,
		    smartSpeed:700,
		    autoWidth:true,
		    responsive:{
		        0:{
		            items:1,
		            margin:5,
		        },
		        320:{
		            items:1,
		            margin:5,
		        },
		        350:{
		            items:3,
		            margin:10,
		        },
		        769:{
		            items:3,
		            margin:10,
		        },
		        1000:{
		            items:7,
		            margin:10,
		        }
		    }
		});
		
		nextArrow.click(function(){
			slider.trigger('next.owl.carousel');
		});
		
		
		prevArrow.click(function(){
			slider.trigger('prev.owl.carousel');
		});




	});	
}





$(document).ready(function(){
	initAptekaSlider();
});




		

	







