$(document).ready(function(){
		 
		 if(navigator.cookieEnabled === true){
			if($.cookie('disksman_popup') === undefined ||$.cookie('disksman_popup') !== "true"){
				var modalPopup =  '<div id="disksman_popup" class="collapse in show">';
					modalPopup +=  '<div class="content clearfix">';
					modalPopup +=  '<p class="text">Продолжая использовать наш сайт, вы даете согласие на обработку файлов cookie и пользовательских данных: сведения о местоположении; тип и версия ОС; тип и версия браузера; тип устройства и разрешение его экрана; источники входа на сайт; язык ОС и браузера; посещенные страницы сайта и действия на них; ip-адрес в целях функционирования сайта, проведения ретаргетинга, проведения статистических исследований и передачи третьим лицам. Если вы не хотите, чтобы ваши данные обрабатывались, покиньте сайт.</p>';		  
					modalPopup +=  '<button type="button" class="close" data-toggle="collapse" data-target="#disksman_popup">Ок</button>';  
					modalPopup +=  '</div>';
					modalPopup +=  '</div>';	
				
				$('body').prepend(modalPopup);
			
				$(document).on('click', '#disksman_popup .close', function(){
					$.cookie('disksman_popup', "true", { expires: 360 });	
				});
			}
		}
		 
		 var where_buy_close = document.querySelector(".close");
		  var where_buy_form = document.querySelector(".where-buy__wrapper");  
		  where_buy_close.addEventListener('click', function(e){
		    e.preventDefault(); 
		    where_buy_form.remove();
		
		});
 });